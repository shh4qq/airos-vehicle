# AIROS Vehicle 开发者手册

## AIROS Vehicle 技术开发框架

![airos-design](/doc/pic/airos-design.png)

## AIROS Vehicle 主要功能模块

![airos-vehicle-dev](/doc/pic/airos-vehicle-dev.png)

## AIROS Vehicle 使用V2X协议栈
V2X协议栈实现了V2X技术标准协议的功能，支持四跨协议(T/CASE-53 2020)
和新四跨协议(YD/T-3709 2020)。V2X协议栈共分为三个层级，分别为适配层、网络层和消息层。
层级内紧凑，层级间独立,层次分明。API清晰易用，开发者可以快速上手开发，事半功倍大大缩短开发周期。

#### 消息层：兼容多种V2X标准协议

* src/asn-wrapper/include/v2x-asn-msgs-adapter.hpp
* src/asn-wrapper/include/v2x-asn-msgs-common.hpp
* src/asn-wrapper/include/v2x-asn-msgs-module.h.in
* src/asn-wrapper/proto/*.proto

#### 网络层: 标准V2X的ASN消息进行DSMP的编码和解码

* src/pc5/ltevdsmp.h
* src/pc5/ltevdsmp.cc

#### 适配层：适配不同的模组

* src/pc5/module_interface.h

#### 使用示例

* 单独使用 src/pc5/test/main.cpp
* 框架内使用 src/components/pc5_service/pc5_service.cc

## AIROS Vehicle 使用开发框架进行二次开发

框架设计类似于微内核的设计理念，它本身只管消息传递和模块调度;
可以通过编写子模块的方式进行扩展，每个模块都有一个唯一的名字，模块间通过指定名字和消息进行通信。

![framework](/doc/pic/framework-dev.png)

### 术语介绍

#### Actor

Actor是基础的执行单元，基于消息驱动

* 驱动类型：消息驱动（被动执行）
* 并发类型：单个Actor串行执行，多个Actor并行执行
* 通信方式：接收消息，发送消息

#### Worker

Worker是一个独立执行的线程，可以与airos-vehicle单向通信

* 驱动类型：自驱动（主动执行）
* 并发类型：单个Worker串行执行，多个Worker并行执行
* 通信方式：接收消息或者发送消息

#### Service

Service由任意个Actor和Worker组成，通过描述文件展现;
它描述框架需要加载的库，需要创建的实例以及实例名称。
例如下面这个例子:

```json
{
  "type": "library",
  "lib": "libHello.so",
  "actor": {
    "HelloActor": [
      {
        "instance_name": "1",
        "instance_params": ""
      }
    ]
  },
  "worker": {
    "HelloReceiver": [
      {
        "instance_name": "1"
      }
    ],
    "HelloSender": [
      {
        "instance_name": "1"
      }
    ]
  }
}
```

* "type":"library": 服务通过库的形式提供
* "lib":"libHello.so": 需要加载的库名称是libHello.so
* 创建1个actor实例，名称是 actor.HelloActor.1
* 创建1个worker实例，名称是 worker.HelloReceiver.1
* 创建1个worker实例，名称是 worker.HelloSender.1

#### Module/Component

通常代指Actor或者Worker

#### Dataflow

Dataflow是一个数据流配置文件；
用来描述每个模块的数据流向。

```py
# 名称为 worker.FakeObuReceiver.FakeObuReceiver 的worker产生的消息
# 需要发送给 actor.SceneService.SceneService
data_flow
{
    name: "worker.FakeObuReceiver.FakeObuReceiver"
    dst: "actor.SceneService.SceneService"
}
# 名称为 actor.VehicleBasicInfoApi.VehicleBasicInfoApi 的actor产生的消息
# 需要发送给 worker.NetServiceSender.NetServiceSender
data_flow
{
    name: "actor.VehicleBasicInfoApi.VehicleBasicInfoApi"
    dst: "worker.NetServiceSender.NetServiceSender"
}
```

### 开发
使用子模块的方式进行二次开发。

#### 创建子模块工程

```bash
python ~/airos-vehicle/tools/gen_proj.py --name="Hello" --dir="/full/proj/path"
```

#### 子模块工程目录说明

类：

+ ```TemplateRecvier```：接收来自模组的消息
+ ```TemplateSender```：发送消息给模组
+ ```TemplateActor```：场景逻辑实现

配置文件：

+ ```Template.json```：Service配置

#### 安装

1. 通过```make install```安装到airos-vehicle目录中
2. 修改```~/airos-vehicle/conf/air_link.dataflow```：配置模块间数据流向

## AIROS Vehicle 实现的V2X场景

### 说明

+ 本次开源内容主要是airos-vehicle框架和v2x协议栈，依赖airos-vehicle框架实现的红绿灯相关场景（绿波车速引导和闯红灯预警）目前只是场景功能的 **简单** 实现。
+ 在理想的条件下，红绿灯相关场景触发应具有精准的定位和能真实准确反映出路口道路信息的地图； **在定位和地图不满足上述要求的情况下，场景触发效果将受到影响** 。
+ 目前场景 **仅支持车道为直道的情况** ，即非端点的所有车道中心线拟合点需在其前后两个拟合点连线左右的二分之一车道宽度范围内。
+ 对于主车所在车道对应多个转向（例如主车位于左直车道）的情况下，场景触发需要满足主车所在车道对应所有方向红绿灯灯色和倒计时 **完全相同** 的条件。

### 概述

![](/doc/pic/scene_framework.png "scene framework") 


目前AIROS Vehicle实现的V2X场景包括:

+ 基础模块 ```HV In Map```: 负责获取主车在地图上定位
+ 绿波车速引导（```GLOSA```: Green Light Optimal Speed Advisory）
+ 闯红灯预警（```RLVW```：Red light Violation Warning）

场景输入：

+ 本车```BSM```
+ 路侧```MAP``` ```SPAT```

场景输出：

+ ```GLOSA``` 的绿波引导 **速度区间** 和 **推荐速度**
+ ```RLVW``` 的 **闯红灯预警等级**

### 消息校验

#### 消息合理性检查

+ 检查 ```MAP``` 消息中字段是否缺失，车道 ```PointList``` 中位置点个数是否在 ```[2,31]```
  区间内，以及通过判断所有非端点的所有车道中心线拟合点是否在其前后两个拟合点连线左右的二分之一车道宽度范围内，若不满足条件则认为是弯道。
+ 检查 ```SPAT``` 消息中灯色倒计时是否缺失，倒计时是否合理。
+ 检查本车 ```BSM``` 消息中字段是否缺失。

#### 消息匹配

+ 本车```BSM```消息认为始终存在。
+ 接收到```MAP```消息后对```MAP```消息进行缓存，并调用```Hv In Map```模块判断```BSM```与```MAP```消息是否匹配，若不匹配则丢弃```MAP```消息。
+ 接收到```SPAT```消息后，若存在缓存的```MAP```消息，则利用```SPAT```和```MAP```消息中的```Phase Id```进行匹配，获取主车所在车道对应红绿灯相位。

### Hv In Map模块

#### 场景描述

计算主车在```MAP```中的位置，即主车所在的```Map Link```、```Map Lane```和```Map Node```。

#### 实现

![](/doc/pic/hv_in_map.png "hv in map") 


```Hv In Map```模块用于在绿波车速引导和闯红灯预警场景中获取主车在地图中的位置。
```BSM```为本车```BSM```，认为始终存在，所以，接收到```MAP```消息即触发```Hv In Map```。
```Hv In Map```模块输入是主车```BSM```和```MAP```，遍历```MAP```中的```Lane```，若满足下列条件则可认为主车位于该车道上，同时```Hv In Map```模块输出主车所在的```Lane```、```Link```和```Node```：

+ ```Lane```上存在与主车距离小于车道宽度的一半(默认值1.5米)的```Position```，存在多个满足情况的车道时取距离最小的车道。
+ 主车航向与车道中心线夹角小于45°。

### 绿波车速引导 GLOSA

#### 场景描述

绿波车速引导 (```GLOSA```: Green Light Optimal Speed Advisory)是指，当装载车载单元(```OBU```)
的主车驶向信号灯控制交叉路口，收到由路侧单元(```RSU```)
发送的道路数据及信号灯实时状态数据时，```GLOSA```应用将给予驾驶员一个建议车速区间，以使车辆能够经济地、舒适地(不需要停车等待)
通过信号路口。本应用适用于城市及郊区普通道路信号灯控制路口。

#### 实现

![](/doc/pic/glosa.png "glosa") 


绿波车速引导模块输入是主车```BSM```、```MAP```和```SPAT```消息，通过如下步骤实现该场景：

+ 通过```Hv In Map```模块获取主车所在```Lane```。
+ 通过匹配```MAP```和```SPAT```中的```Region ID```和```Node ID```，判断```SPAT```和```MAP```消息是否匹配，不匹配则不触发场景。
+ 计算主车当前位置到所在```Lane```停止线中心点的距离，及主车以当前速度行驶至停止线所需的时间。
+ 与```SPAT```消息中的灯色倒计时进行比较，给出建议速度区间和推荐速度。

#### 计算主车与停止线的距离

![](/doc/pic/calc_distance.png "calculate distance") 


虽然目前场景的实现还不支持弯道的情况，但在计算距离时将所有直道都看作弯道处理可以便于后续版本迭代。 


如图所示，从主车的位置到停止线（车道中心线拟合点中的最后一个点)认为所有车道都是弯道（直道是角度无限趋近于0的弯道）。 


利用下列公式可以计算得出当前主车所在位置到停止线中心点的距离，其中$\gamma$时主车在两个位置的航向角之差。
$$ \Delta D = R \gamma \\ R = \dfrac{D}{2sin\dfrac{\gamma}{2}} $$

#### 触发场景条件

计算得出主车当前位置到停止线中心点的距离后，通过主车速度可以计算得出主车以当前速度行驶至停止线中心点所需的时间，通过与当前车道对应红绿灯的灯色倒计时比较，如果满足下列条件则触发场景：

+ 主车所在车道对应所有灯色一致且倒计时完全相同。
+ 主车以当前速度行驶至车道停止线时需要停车等待。

#### 确定最大速度和最小速度

| 是否触发场景 | 当前灯色     | 速度上限                                             | 速度下限                                         | 备注          |
|--------|------------|--------------------------------------------------|----------------------------------------------|-------------|
| 否      | -          | -                                                | -                                            | 当前速度可正常通过路口 |
| 是      | 绿灯-加速通过    | 道路最大限速      | 距离 / 当前绿灯倒计时结束时间  |             |
| 是      | 绿灯-下一个绿灯通过 | min{距离/下一个绿道倒计时结束时间, 道路最大限速} | 0                                            |                  |
| 是      | 绿灯-立即停车    | 0                                                | 0                                            | 绿灯倒计时为0      |
| 是      | 红灯         | min{距离/下一个绿道倒计时结束时间, 道路最大限速} | 0                                            |             |
| 是      | 黄灯         | min{距离/下一个绿道倒计时结束时间, 道路最大限速} | 0                                            |             |
| 是      | 黑灯         | 道路最大限速                                           | 0                                            |             |

#### 推荐速度

推荐速度值：

+ 可绿灯通过的速度区间中 **5的倍数** 的中间值；
+ 若速度区间中不含 **5的倍数** ，则取区间最大值向上最大的5的整倍数。

例如:

+ 速度区间为```[37，54]```，则推荐绿波车速为```45```
+ 速度区间为```[47，49]```，则推荐绿波车速为```50```。

### 闯红灯预警 RLVW

#### 场景描述

闯红灯预警 (```RLVW```: Red Light Violation Warning)是指，主车(Host Vehicle)经过有信号控制的交叉口(车道)
，车辆存在不按信号灯规定或指示行驶的风险时，RLVW应用对驾驶员进行预警。本应用适用于城市及郊区道路及公路的交叉路口、环道的出入口和可控车道、高速路入口和隧道等有信号控制的车道。

#### 实现

![](/doc/pic/rlvw.png "rlvw")
闯红灯预警模块输入是主车```BSM```、```MAP```和```SPAT```消息，通过如下步骤实现该场景：

+ 通过```Hv In Map```模块获取主车所在车道
+ 通过匹配```MAP```和```SPAT```中的```Region Id```和```Node Id```，判断```SPAT```和```MAP```消息是否匹配，不匹配则不触发场景
+ 计算主车当前位置到所在车道停止线中心点的距离，及主车以当前运动状态（包括速度和加速度）行驶至停止线所需的时间
+ 与```SPAT```消息中的灯色倒计时进行比较，给出闯红灯预警等级

#### 计算主车与停止线的距离

实现方式同绿波车速引导中计算主车与停止线距离的部分。

#### 场景触发条件

在上述基础上，通过主车速度可以计算得出主车以当前速度行驶至停止线中心点所需的时间，通过与当前车道对应红绿灯的灯色倒计时比较，如果满足下列条件则触发场景：

+ 主车所在车道对应所有灯色一致且倒计时完全相同。
+ 主车以当前运动状态（包括速度和加速度）行驶至车道停止线时需要停车等待。 


  在绿波车速引导的基础上考虑了加速度的影响，因为绿波车速场景触发即加速度改变，而闯红灯预警触发是根据车辆当前运动状态判断主车通过路口时是否存在闯红灯风险。

#### 确定闯红灯风险等级

 | 是否触发场景 | 条件                                     | 风险等级             |
|--------|----------------------------------------|------------------|
| 否      | 主车保持当前运动状态可正常通过路口                      | No Severity      |
| 是      | 主车以当前运动状态通过路口有闯红灯风险，且主车行驶至车道停止线时间大于10秒 | Severity Notify  |
| 是      | 主车以当前运动状态通过路口有闯红灯风险，且主车行驶至车道停止线时间小于10秒 | Severity Warning |


