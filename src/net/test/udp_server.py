#!/usr/bin/env python2
# -*- coding: utf-8 -*-

import socket
import time
from google.protobuf import message
from v2xpb_asn_message_frame_pb2 import MessageFrame
from v2xpb_asn_bsm_pb2 import Bsm

local_addr = ("127.0.0.1", 8765)
dst_addr = ("127.0.0.1", 5678)
server_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
server_socket.bind(local_addr)

def build_bsm():
    msgframe = MessageFrame()
    bsm = msgframe.bsmFrame
    bsm.msg_cnt = 10
    bsm.id = r'mkz134  '
    pb = msgframe.SerializeToString()
    return pb

while True:
    server_socket.sendto(build_bsm(), dst_addr)
    time.sleep(1)
