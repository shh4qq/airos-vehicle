/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "framework/log.h"

#include <string>

#include "framework/flags.h"

static void SignalHandler(const char *data, size_t size) {
  std::string str = std::string(data, size);
  LOG(ERROR) << str;
}

namespace air {
namespace link {

Log::Log() {
  // output log immediately
  FLAGS_logbufsecs = 0;
  // set the log file to 100MB
  FLAGS_max_log_size = 100;
  FLAGS_stop_logging_if_full_disk = true;
  // install core handle
  google::InstallFailureSignalHandler();
  google::InstallFailureWriter(&SignalHandler);
  // init glog
  google::InitGoogleLogging("air_link");
  // log with level >=ERROR is output to stderr
  google::SetStderrLogging(google::GLOG_FATAL);
  // set the path for the log file
  std::string dest_dir = air::link::FLAGS_air_link_log_dir + "/info";
  google::SetLogDestination(google::GLOG_INFO, dest_dir.c_str());
}

}  // namespace link
}  // namespace air
