/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#pragma once

#include <memory>

#include "framework/common.h"
#include "framework/worker.h"

namespace air {
namespace link {

class Msg;
class Context;
class WorkerCommon final : public Worker {
  friend class App;

 public:
  WorkerCommon();
  ~WorkerCommon();

  /// override Worker virtual method
  void Run() override;
  void OnInit() override;
  void OnExit() override;

  /// override Event virtual method
  EventType GetEventType() { return EventType::WORKER_COMMON; }

  void SetContext(std::shared_ptr<Context> context) { context_ = context; }
  std::shared_ptr<Context> GetContext() {
    return (context_.expired() ? nullptr : context_.lock());
  }

 private:
  /* 工作线程消息处理 */
  int Work();
  /* 工作线程进入空闲链表之前进行的操作 */
  void Idle();
  //// 当前执行actor的指针
  std::weak_ptr<Context> context_;
};

}  // namespace link
}  // namespace air
