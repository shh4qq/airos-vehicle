/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "framework/mod_manager.h"

#include <json/json.h>

#include "framework/actor.h"
#include "framework/log.h"
#include "framework/worker.h"

namespace air {
namespace link {

ModManager::ModManager() {
  pthread_rwlock_init(&class_actor_rw_, NULL);
  pthread_rwlock_init(&class_worker_rw_, NULL);
}

ModManager::~ModManager() {
  pthread_rwlock_destroy(&class_actor_rw_);
  pthread_rwlock_destroy(&class_worker_rw_);
}

bool ModManager::LoadMod(const std::string& dl_path) {
  return lib_mods_.LoadMod(dl_path);
}

bool ModManager::RegActor(
    const std::string& class_name,
    std::function<std::shared_ptr<Actor>(const std::string&)> func) {
  pthread_rwlock_wrlock(&class_actor_rw_);
  if (class_actors_.find(class_name) != class_actors_.end()) {
    pthread_rwlock_unlock(&class_actor_rw_);
    LOG(WARNING) << "reg " << class_name << " failed, "
                 << " has exist";
    return false;
  }
  class_actors_[class_name] = func;
  pthread_rwlock_unlock(&class_actor_rw_);
  return true;
}

bool ModManager::RegWorker(
    const std::string& class_name,
    std::function<std::shared_ptr<Worker>(const std::string&)> func) {
  pthread_rwlock_wrlock(&class_worker_rw_);
  if (class_workers_.find(class_name) != class_workers_.end()) {
    pthread_rwlock_unlock(&class_worker_rw_);
    LOG(WARNING) << "reg " << class_name << " failed, "
                 << " has exist";
    return false;
  }
  class_workers_[class_name] = func;
  pthread_rwlock_unlock(&class_worker_rw_);
  return true;
}

std::shared_ptr<Actor> ModManager::CreateActorInst(
    const std::string& mod_or_class_name, const std::string& actor_name) {
  if (lib_mods_.IsLoad(mod_or_class_name)) {
    LOG(INFO) << "instance actor from lib";
    return lib_mods_.CreateActorInst(mod_or_class_name, actor_name);
  }
  pthread_rwlock_rdlock(&class_actor_rw_);
  if (mod_or_class_name == "class" &&
      class_actors_.find(actor_name) != class_actors_.end()) {
    LOG(INFO) << "instance actor from reg class";
    auto actor = class_actors_[actor_name](actor_name);
    actor->SetModName(mod_or_class_name);
    actor->SetTypeName(actor_name);
    pthread_rwlock_unlock(&class_actor_rw_);
    return actor;
  }
  pthread_rwlock_unlock(&class_actor_rw_);
  return nullptr;
}

std::shared_ptr<Worker> ModManager::CreateWorkerInst(
    const std::string& mod_or_class_name, const std::string& worker_name) {
  if (lib_mods_.IsLoad(mod_or_class_name)) {
    LOG(INFO) << "instance worker from lib";
    return lib_mods_.CreateWorkerInst(mod_or_class_name, worker_name);
  }
  pthread_rwlock_rdlock(&class_worker_rw_);
  if (mod_or_class_name == "class" &&
      class_workers_.find(worker_name) != class_workers_.end()) {
    LOG(INFO) << "instance worker from reg class";
    auto worker = class_workers_[worker_name](worker_name);
    worker->SetModName(mod_or_class_name);
    worker->SetTypeName(worker_name);
    pthread_rwlock_unlock(&class_worker_rw_);
    return worker;
  }
  pthread_rwlock_unlock(&class_worker_rw_);
  return nullptr;
}

}  // namespace link
}  // namespace air
