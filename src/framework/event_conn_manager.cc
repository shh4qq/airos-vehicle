/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "framework/event_conn_manager.h"

#include <glog/logging.h>

#include "framework/app.h"
#include "framework/event_conn.h"

namespace air {
namespace link {

EventConnManager::EventConnManager() {}
EventConnManager::~EventConnManager() {
  std::lock_guard<std::mutex> g(mtx_);
  auto app = app_.lock();
  if (app == nullptr) {
    LOG(ERROR) << "app is nullptr";
    return;
  }
  for (auto p : run_conn_) {
    app->DelEvent(p.second);
  }
  run_conn_.clear();
  run_conn_map_.clear();
  idle_conn_.clear();
}

bool EventConnManager::Init(std::shared_ptr<App> app, int sz) {
  app_ = app;
  for (int i = 0; i < sz; ++i) {
    std::lock_guard<std::mutex> g(mtx_);
    AddEventConn();
  }
  return true;
}

void EventConnManager::AddEventConn() {
  auto conn = std::make_shared<EventConn>();
  std::string name = "event.conn." + std::to_string(conn_sz_);
  conn->SetEvConnName(name);
  idle_conn_.emplace_back(conn);
  conn_sz_++;
}

std::shared_ptr<EventConn> EventConnManager::Get(int handle) {
  std::lock_guard<std::mutex> g(mtx_);
  if (run_conn_map_.find(handle) == run_conn_map_.end()) {
    DLOG(WARNING) << "can't find event conn, handle " << handle;
    return nullptr;
  }
  auto name = run_conn_map_[handle];
  if (run_conn_.find(name) == run_conn_.end()) {
    DLOG(WARNING) << "can't find event conn, name " << name;
    return nullptr;
  }
  return run_conn_[name];
}

std::shared_ptr<EventConn> EventConnManager::Get() {
  std::lock_guard<std::mutex> g(mtx_);
  // check has event conn
  if (idle_conn_.empty()) {
    AddEventConn();
  }
  auto app = app_.lock();
  if (app == nullptr) {
    LOG(ERROR) << "app is nullptr";
    return nullptr;
  }
  // remove from idle_conn
  auto conn = idle_conn_.front();
  idle_conn_.pop_front();
  // add to run_conn
  run_conn_[conn->GetEvConnName()] = conn;
  run_conn_map_[conn->GetFd()] = conn->GetEvConnName();
  // add to epoll
  app->AddEvent(conn);
  return conn;
}

void EventConnManager::Release(std::shared_ptr<EventConn> ev) {
  std::lock_guard<std::mutex> g(mtx_);
  auto app = app_.lock();
  if (app == nullptr) {
    LOG(ERROR) << "app is nullptr";
    return;
  }
  // delete from epoll
  app->DelEvent(ev);
  // remove from run_conn
  auto name = ev->GetEvConnName();
  run_conn_.erase(name);
  run_conn_map_.erase(ev->GetFd());
  // add to idle_conn
  idle_conn_.emplace_back(ev);
}

// call by main frame
void EventConnManager::Notify(const std::string& name,
                              std::shared_ptr<Msg> msg) {
  std::shared_ptr<EventConn> ev = nullptr;
  {
    std::lock_guard<std::mutex> g(mtx_);
    if (run_conn_.find(name) == run_conn_.end()) {
      LOG(WARNING) << "can't find " << name;
      return;
    }
    ev = run_conn_[name];
  }
  // push msg to event_conn
  ev->recv_.emplace_back(msg);
  // send cmd to event_conn
  ev->SendCmdToWorker(WorkerCmd::RUN);
}

}  // namespace link
}  // namespace air
