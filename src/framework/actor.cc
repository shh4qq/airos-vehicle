/******************************************************************************
 * Copyright 2022 The Airos Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *****************************************************************************/

#include "framework/actor.h"

#include "framework/app.h"
#include "framework/context.h"
#include "framework/worker_timer.h"

namespace air {
namespace link {

Actor::Actor() {}

Actor::~Actor() {}

void Actor::SetModName(const std::string& name) {
  if (mod_name_ == "class") {
    is_from_lib_ = false;
  } else {
    is_from_lib_ = true;
  }
  mod_name_ = name;
}

int Actor::Send(const std::string& dst, std::shared_ptr<Msg> msg) {
  if (ctx_.expired()) {
    return -1;
  }
  msg->SetSrc(GetActorName());
  msg->SetDst(dst);
  return ctx_.lock()->SendMsg(msg);
}

const std::string Actor::GetActorName() const {
  return "actor." + actor_name_ + "." + instance_name_;
}

int Actor::Timeout(const std::string& timer_name, int expired) {
  if (ctx_.expired()) {
    return -1;
  }
  return ctx_.lock()->GetApp()->GetTimerWorker()->SetTimeout(
      GetActorName(), timer_name, expired);
}

void Actor::SetContext(std::shared_ptr<Context> c) { ctx_ = c; }

}  // namespace link
}  // namespace air
