CMAKE_MINIMUM_REQUIRED(VERSION 3.16)

### source
FILE(GLOB SCENE_SERVICE_SRCS "*.cpp" "*.cc" "*.c")

### lib
ADD_LIBRARY(air_scene_service OBJECT ${SCENE_SERVICE_SRCS})
TARGET_LINK_LIBRARIES(air_scene_service
    air-link-asn-wrapper-grpc_pb
)
