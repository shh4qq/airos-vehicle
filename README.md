# AIROS Vehicle

## 介绍
airos-vehicle是面向车端提供的c-v2x车路协同应用框架，开发者可基于应用框架方便简单的开发车端v2x应用，具备以下特性：

+ 支持C-V2X标准协议栈
+ 支持四跨和新四跨
+ 支持Linux和Android操作系统
+ 支持x86_64、aarch64 CPU架构

AIROS Vehicle也可用于T-Box、OBU、RSU等通信设备中实现C-V2X标准协议栈

## 部署编译环境

1. 安装```podman```,```buildah``` 或者```docker```其中一种
    1. [podman  官方安装文档](https://podman.io/getting-started/installation)
    2. [buildah 官方安装文档](https://github.com/containers/buildah/blob/main/install.md)
    3. [docker  官方安装文档](https://docs.docker.com/engine/install/)
2. 运行 ```docker/build-env-create.bash``` 创建镜像
3. 使用 ```docker/build-env-enter.bash``` 进入编译环境

### 编译环境预置的依赖库

如果不希望使用container进行程序的编译与部署，可以考虑手动补足以下依赖库

+ openssl
    + crypto
    + ssl
+ google-glog
+ google-gflags
+ google-test
+ jsoncpp
+ websocketpp
+ boost-system
+ GeographicLib

## 编译/裁剪/安装/运行

```bash
# 初始化 submodule
git submodule update --init

# 如果已经完成 submodule 初始化,可以使用下列命令进行更新
git submodule update --checkout

# airos-vehicle默认会启动自带的模块，可以通过编译选项不启动
# -DUSE_FAKE_OBU=OFF
#   不启动模拟与RSU通信的模块
# -DUSE_SCENE_SERVICE=OFF
#   不启动airos-vehicle中实现的V2X场景
cmake -DCMAKE_MODULE_PATH=/usr/share/cmake/geographiclib/ \
      -S . -B build

# 目前安装路径被指定为 ${HOME},
# 如果需要修改安装位置,请手动修改CMakeLists.txt
make -C build -j "$(nproc)" install

# 启动AIROS Vehicle的demo程序
${HOME}/airos-vehicle/bin/airos-vehicle \
    --flagfile=${HOME}/airos-vehicle/conf/air_link.conf
```

## 产出目录说明

```
.
├── bin                # AIROS Vehicle Demo程序
├── conf               # AIROS Vehicle配置文件
├── include            # AIROS Vehicle开发头文件
├── lib                # AIROS Vehicle库，依赖库和子模块库
├── log                # 日志目录
├── proto              # 消息协议proto
├── service            # 服务模块描述文件
├── templates          # 模版工程
└── tools              # 生成子模块模版工程脚本
```

## AIROS Vehicle 开发
[AIROS Vehicle开发手册](doc/airos_vehicle_man.md)

## 其它
